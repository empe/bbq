from datetime import datetime, timedelta
from jinja2.sandbox import SandboxedEnvironment
from jinja2 import Template


def offset(date, days=0, hours=0, minutes=0):
    return date + timedelta(days=days, hours=hours, minutes=minutes)


def parse_date(date_string, fmt='%Y-%m-%d'):
    datetime.strptime(date_string, format=fmt)


def format_date(date, fmt='%Y-%m-%d'):
    return datetime.strftime(date, format=fmt)


class Formatter(SandboxedEnvironment):
    """ Sandboxed environment that prevents XSS allowing to apply only defined formatters """

    def __init__(self, *args, **kwargs):
        super(Formatter, self).__init__(*args, **kwargs)
        self.filters.update({
            'offset': offset,
            'parse': parse_date,
            'format': format_date
        })

    def is_safe_callable(self, obj):
        return False

    def render(self, template, **params):
        tpl = self.from_string(template, globals={'now': datetime.now()})
        return tpl.render(**params)
