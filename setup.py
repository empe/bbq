#!/usr/bin/env python

from os.path import exists
from setuptools import setup

setup(
    name='bbq',
    version=open('VERSION').read().strip(),
    author='Mateusz Pytel',
    author_email='mateusz.pytel.87@gmail.com',
    packages=['bbq'],
    scripts=[],
    url='https://gitlab.com/empe/bbq',
    license="Apache License v2",
    description='GOOGLE Big Query command line nutritions',
    long_description=open('README.md').read() if exists("README.md") else "",
    install_requires=[
        'google-cloud-bigquery>=0.27.0',
        'jinja2>=2.9.0',
        'pyaml>=17.10.0'
    ],
    setup_requires=['pytest-runner'],
    tests_require=['pytest', 'pytest-cov'],
    include_package_data=True,
)
