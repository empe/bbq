import datetime

from bbq.format import Formatter


class TestFormatter(object):

    @classmethod
    def setup_class(cls):
        cls.formatter = Formatter()

    def test_globals_should_have_now(self):
        assert self.formatter.render("{{ now | format }}") == datetime.datetime.now().strftime(format='%Y-%m-%d')
