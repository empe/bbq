FROM python:2.7

RUN mkdir -p /home
WORKDIR /home

RUN pip install --no-cache-dir bbq --extra-index-url=https://packagecloud.io/EMPE/data-tools/pypi/simple
